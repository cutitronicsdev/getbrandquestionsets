import traceback
import logging
import pymysql
import json
import uuid
import sys
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(400)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500


def fn_getQuestionSets(lv_CutitronicsBrandID):

    lv_list = []
    lv_statement = "SELECT DISTINCT(QuestionSet) FROM ClientSurveyQuestion WHERE CutitronicsBrandID = %(CutitronicsBrandID)s;"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})

            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))
            
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def lambda_handler(event, context):
    """
    getQuestionSet

    Gets HAYGO Question set for a Brand
    """
    openConnection()

    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Return block

    getQuestionSet_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}


    try:
        # Variables
        body = event.get('multiValueQueryStringParameters')

        lv_CutitronicsBrandID = body.get('CutitronicsBrandID')[0]

        '''
        1. Does Brand Exist?
        '''

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID)

        if lv_BrandType == 400:  # Failure

            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            getQuestionSet_out['Status'] = "Error"
            getQuestionSet_out['ErrorCode'] = context.function_name + "_001"  # getBrandProducts_out_001
            getQuestionSet_out['ErrorDesc'] = "Supplied brandID does not exist in the BO Database"

            # Lambda response
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getQuestionSet_out)
            }

        '''
        2. Does brand have HAYGO Questions set up?
        '''

        lv_QuestionSetType = fn_getQuestionSets(lv_CutitronicsBrandID)

        if lv_QuestionSetType == 400 or lv_QuestionSetType == 500:  # Failure

            logger.error("Cannot find any questions for brand '{lv_CutitronicsBrandID}' in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            getQuestionSet_out['Status'] = "Error"
            getQuestionSet_out['ErrorCode'] = context.function_name + "_002"  # getBrandProducts_out_001
            getQuestionSet_out['ErrorDesc'] = "Supplied brandID does not have any HAYGO Questions"

            # Lambda response
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getQuestionSet_out)
            }
        
        '''
        3. Build output
        '''

        lv_QuestionSets = []

        for lv_QuestionSet in lv_QuestionSetType:
            lv_QuestionSets.append(lv_QuestionSet.get('QuestionSet'))

        Connection.close()

        getQuestionSet_out['Status'] = ""
        getQuestionSet_out['ErrorCode'] = context.function_name + "_000"  # getBrandProducts_out_001
        getQuestionSet_out['ErrorDesc'] = ""
        getQuestionSet_out['ServiceOutput']['QuestionSets'] = lv_QuestionSets


        return {
                "isBase64Encoded": "false",
                "statusCode": 200,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getQuestionSet_out)
            }


    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)

        Connection.rollback()
        Connection.close()

        # Populate Cutitronics reply block

        getQuestionSet_out['Status'] = "Error"
        getQuestionSet_out['ErrorCode'] = context.function_name + "_000"  # CreateTherapistSession_000
        getQuestionSet_out['ErrorDesc'] = "CatchALL"
        getQuestionSet_out["ServiceOutput"]["CutitronicsBrandID"] = body.get('CutitronicsBrandID', "")

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getQuestionSet_out)
        }
        
# class context:
#     def __init__(self):
#         self.function_name = "getBrandQuestionSets"

# context = context()
# event = {'multiValueQueryStringParameters': {'CutitronicsBrandID': ["0002"]}}

# x = lambda_handler(event, context)
# print(x)